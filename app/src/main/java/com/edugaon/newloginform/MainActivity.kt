package com.edugaon.newloginform

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn=findViewById<Button>(R.id.btn)as Button
        val email:EditText=findViewById(R.id.email)as EditText
        val password:EditText=findViewById(R.id.password)as EditText

        btn.setOnClickListener {
            if (email.toString() == "" &&
                password.toString() == ""
            )
                Toast.makeText(this, "login success", Toast.LENGTH_LONG).show()

        else{
                Toast.makeText(this,"loginfail", Toast.LENGTH_LONG).show()
                val intent =Intent(this,Secondactivity2::class.java)
                startActivity(intent)
            }

        }
    }
}